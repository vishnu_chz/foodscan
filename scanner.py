import warnings
import time
import numpy as np
import cv2
from hx711 import HX711
from  tensorflow import keras

np.random.seed(1)
warnings.filterwarnings('ignore')

def analyze():
    hx = HX711(5, 6)
    hx.set_reading_format("MSB", "MSB")
    hx.set_reference_unit(324)
    hx.reset()
    hx.tare()
    print("Add weight...")
    val = hx.get_weight(5)
    hx.power_down()
    hx.power_up()
    time.sleep(0.2)
    weight = 0.0
    while True:
        val1 = hx.get_weight(5)
        hx.power_down()
        hx.power_up()
        time.sleep(0.2)
        if val1-val>20:
            v0 =  hx.get_weight(5)
            hx.power_down()
            hx.power_up()
            time.sleep(0.1)
            weight =  hx.get_weight(5)
            hx.power_down()
            hx.power_up()
            time.sleep(0)
            print(weight)
            break

    # reading cam
    cam = cv2.VideoCapture(0)
    ret, image = cam.read()
    # cv2.imshow('Imagetest',image)
    time.sleep(2)
    cv2.imwrite('testimage.jpg', image)
    cam.release()
    cv2.destroyAllWindows()

    # model
    model = keras.models.load_model('./model_weights')
    test_images = []
    shape = (200,200)
    img = cv2.imread('testimage.jpg')
    img = cv2.resize(img,shape)

    test_images=[]
    test_images.append(img)
    test_images=np.array(test_images)
    test_images=test_images[0:1]
    predict = model.predict(np.array(test_images))
    output = { 0:'apple',1:'banana',2:'mixed',3:'orange'}
    return {"name":output[np.argmax(predict)],"weight":weight}
