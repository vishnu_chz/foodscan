from sqlalchemy import Column, DateTime, Float, ForeignKey, Integer, String
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func
from database import Base,DB_ENGINE

def init_tables():
    Base.metadata.create_all(bind=DB_ENGINE)

class Fruit(Base):
    __tablename__="fruits"
    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, unique=True, index=True)
    price = Column(Float)
    invoices = relationship("Invoice", back_populates="fruit")


class Bill(Base):
    __tablename__="bills"
    id = Column(Integer, primary_key=True, index=True)
    created_at = Column(DateTime(timezone=True), server_default=func.now())
    updated_at = Column(DateTime(timezone=True), onupdate=func.now())
    invoices = relationship("Invoice", back_populates="bill")


class Invoice(Base):
    __tablename__="invoices"
    id = Column(Integer, primary_key=True, index=True)
    weight = Column(Float)
    fruit_id = Column(Integer, ForeignKey('fruits.id'))
    bill_id = Column(Integer, ForeignKey('bills.id'))
    
    fruit = relationship("Fruit", back_populates="invoices")
    bill = relationship("Bill", back_populates="invoices")

