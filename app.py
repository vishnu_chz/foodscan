import time
from fastapi import  FastAPI ,HTTPException,Depends
from fastapi.middleware.cors import CORSMiddleware
from models import init_tables
from schemas import Fruit,FruitCreate,InvoiceCreate,Bill
from database import SessionLocal,DB_ENGINE
from sqlalchemy.orm import Session
import crud
# from scanner import analyze

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

init_tables()

def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

def long_process():
    time.sleep(3)
    return {"fruit":"banana","weight":10}

@app.post("/fruits",tags=['fruits'])
def create_fruit(fruit:FruitCreate,db:Session = Depends(get_db)):
    try:
        return crud.create_fruit(db,fruit)
    except Exception as e:
        raise HTTPException(status_code=404,detail="fruit exists")

@app.get("/fruits",tags=['fruits'])
async def get_all_fruits(db:Session=Depends(get_db)):
    return crud.get_all_fruits(db)

@app.get("/fruits/{name}",tags=['fruits'])
async def get_fruit_by_name(name:str,db:Session=Depends(get_db)):
    fruit = crud.get_fruit_by_name(db,name)
    if fruit is None:
        raise HTTPException(status_code=404,detail="fruit is not registered")
    else:
        return fruit


@app.post("/bills",tags=["bills"])
async def create_bill(db:Session=Depends(get_db)):
    return crud.create_bill(db)

@app.get("/bills",tags=["bills"])
async def get_all_bills(db:Session=Depends(get_db)):
    return crud.get_all_bills(db)

@app.get("/bills/{id}",tags=["bills"])
async def get_bill_by_id(id:int,db:Session=Depends(get_db)):
    bill = crud.get_bill_by_id(db,id)
    if bill is None:
        raise HTTPException(status_code=404,detail="bill is not found")
    else:
        return bill

@app.post("/invoice/{bill_id}",tags=["invoices"])
async def create_invoice(bill_id:int,db:Session=Depends(get_db)):
    data = long_process()
    # data = analyze()
    weight = round(data["weight"],2)
    fruit:Fruit = crud.get_fruit_by_name(db,data["fruit"])
    if not fruit:
        raise HTTPException(status_code=404, detail="fruit not found")
    bill:Bill = crud.get_bill_by_id(db,bill_id)
    if not bill:
        raise HTTPException(status_code=404,detail="bill not found")
    invoice_data:InvoiceCreate = InvoiceCreate(weight=weight,fruit_id=fruit.id,bill_id=bill_id)
    invoice = crud.create_invoice(db,invoice_data)
    return invoice

@app.get("/invoice/{bill_id}",tags=["invoices"])
async def get_invoice_by_bill(bill_id:int,db:Session=Depends(get_db)):
    return crud.get_invoice_by_bill(db,bill_id)

@app.delete("/invoice/{id}",tags=["invoices"])
async def delete_invoice(id:int,db:Session=Depends(get_db)):
    return crud.delete_invoice(db,id)

@app.get("/bill/{bill_id}/total",tags=["bills"])
async def compute_tital(bill_id:int,db:Session=Depends(get_db)):
    invoices = crud.get_invoice_by_bill(db,bill_id)
    total = 0
    for invoice in invoices:
        weight = invoice.weight
        fruit = invoice.fruit
        total+= (weight/1000)*fruit.price
    return round(total,2)

@app.delete("/bill/{id}",tags=["bills"])
async def delete_ibill(id:int,db:Session=Depends(get_db)):
    return crud.delete_bill(db,id)