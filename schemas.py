from datetime import datetime
from typing import List, Optional
from pydantic import BaseModel

class FruitBase(BaseModel):
    name: str
    price: float

class FruitCreate(FruitBase):
    pass

class Fruit(FruitBase):
    id: int
    invoices: List[str] = []

    class Config:
        orm_mode = True

class BillBase(BaseModel):
    pass

class BillCreate(BillBase):
    pass

class Bill(BillBase):
    id: int
    created_at: Optional[datetime]
    updated_at: Optional[datetime]
    invoices: List[str] = []

    class Config:
        orm_mode = True

class InvoiceBase(BaseModel):
    weight: float
    fruit_id: int
    bill_id: int

class InvoiceCreate(InvoiceBase):
    pass

class Invoice(InvoiceBase):
    id: int
    fruit: Fruit
    bill: Bill

    class Config:
        orm_mode = True

class Total(BaseModel):
    total:int


# Assign the Invoice class to the corresponding string in Fruit and Bill
Fruit.update_forward_refs()
Bill.update_forward_refs()

