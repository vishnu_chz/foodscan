from sqlalchemy.orm import Session,joinedload
from schemas import FruitCreate,InvoiceCreate
from models import Fruit,Bill,Invoice

def create_fruit(db:Session,fruit:FruitCreate):
        db_fruit = Fruit(name=fruit.name, price=fruit.price)
        db.add(db_fruit)
        db.commit()
        db.refresh(db_fruit)
        return db_fruit

def get_all_fruits(db:Session,skip:int=0,limit:int=100):
        return db.query(Fruit).offset(skip).limit(limit).all()

def get_fruit_by_name(db:Session,name:str):
        return db.query(Fruit).filter(Fruit.name == name).first()

def create_bill(db:Session):
    db_bill = Bill()
    db.add(db_bill)
    db.commit()
    db.refresh(db_bill)
    return db_bill

def get_all_bills(db:Session,skip:int=0,limit:int=100):
        return db.query(Bill).offset(skip).limit(limit).all()

def get_bill_by_id(db:Session,id:int):
       return db.query(Bill).filter(Bill.id == id).first()

def create_invoice(db:Session,invoice:InvoiceCreate):
    db_invoice = Invoice(**invoice.dict())
    db.add(db_invoice)
    db.commit()
    db.refresh(db_invoice)
    db_invoice = db.query(Invoice).options(
    joinedload(Invoice.fruit),
    joinedload(Invoice.bill)).filter_by(id=db_invoice.id).one()
    return db_invoice


def get_invoice_by_bill(db:Session,id:int):
    invoices = (
        db.query(Invoice).options(
        joinedload(Invoice.fruit),
        joinedload(Invoice.bill))
        .filter_by(bill_id=id)
        .all())
    return invoices

def delete_invoice(db:Session,id:int):
    db_invoice = db.query(Invoice).filter_by(id=id).first()
    if db_invoice:
        db.delete(db_invoice)
        db.commit()
        return id
    else:
        return -1

def delete_bill(db:Session,id:int):
    db_bill = db.query(Bill).filter_by(id=id).first()
    if db_bill:
        db.delete(db_bill)
        db.commit()
        return id
    else:
        return -1