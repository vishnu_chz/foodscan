import uvicorn

def serve():
    uvicorn.run('app:app', host="0.0.0.0", port=8000)

def dev():
    uvicorn.run('app:app',reload=True, host="0.0.0.0", port=8000)

if __name__=="__main__":
    dev()
