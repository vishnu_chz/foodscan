from setuptools import setup, find_packages

setup(
    name='foodscan',
    version='1.0',
    packages=find_packages(),
    install_requires=[
        # 'fastapi[all]',
        # 'tensorflow',
        # 'opencv-python',
        # 'numpy',
        # 'Rpi.GPIO'
    ],
    entry_points={
        'console_scripts': [
            'server:dev = main:dev',
            'server = main:serve',
            'server:prod = main:serve'
        ]
    },
    author='Your Name',
    author_email='your_email@example.com',
    description='A package for fruit scanning and classification',
    url='https://github.com/your_username/fruitScan',
    )
